<?php
spl_autoload_register(
    function ($className)
    {
        if (class_exists($className)) {
            return;
        }
        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $className.'.php');
        $filePath = "classes/".$fileName;
        if (file_exists($filePath)) {
            require_once($filePath);
            return;
        }
        die("Error: Failed to find $className at $filePath");
    }
    
);

$Demo = new Demo;

?>
<!doctype html>
<html lang="en">
<head>
<title>Needls.com Coding Challenge</title>
<meta charset="utf-8"/>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<link rel=stylesheet href="lib/codemirror.css">
<link rel=stylesheet href="doc/docs.css">
<script src="lib/codemirror.js"></script>
<script src="mode/xml/xml.js"></script>
<script src="mode/javascript/javascript.js"></script>
<script src="mode/css/css.js"></script>
<script src="mode/php/php.js"></script>
<script src="mode/htmlmixed/htmlmixed.js"></script>
<script src="addon/edit/matchbrackets.js"></script>
<script src="mode/clike/clike.js"></script>
<script src="doc/activebookmark.js"></script>

<script>
var php_spec = {
  lineNumbers: true,
  matchBrackets: true,
  mode: "application/x-httpd-php",
  indentUnit: 4,
  indentWithTabs: true
}
</script>

<style>
  .CodeMirror { height: auto; border: 1px solid #ddd; }
  .CodeMirror-scroll { Xmax-height: 400px;}
  .CodeMirror pre { padding-left: 7px; line-height: 1.25; }
  i { color: #f00; }
</style>
</head>
<body>
<div id=nav>
  <a href="http://www.needls.com">
  <img id=logo src="http://www.needls.com/img/needls-logo-white.png"
  style="background:black;width:200px" alt="Needls.com logo"></a>

  <ul>
    <li><a href="#intro1" class=active data-default="true">Coding Challenge</a>
    <li><a href="#intro2">Status Log Data</a>
    <li><a href="#intro3">Start and Stop dates</a>
  </ul>
  <ul>
    <li><a href="#coding1">Coding Instructions</a></li>
    <li><a href="#coding2">Bonus Challenge</a></li>
    <li><a href="#coding3">Reviewer Notes</a></li>
  </ul>

  <ul>
    <li><a href="#data">Sample Data</a>
    <?php print $Demo->drawTestLinks(); ?>
  </ul>

  <ul>
    <li><a href="https://bitbucket.org/classaxe/needls/overview">Obtain Source Code</a>
  </ul>

  <p>
    <a href="https://validator.w3.org/nu/?doc=https://www.classaxe.com/needls&showsource=yes&showoutline=yes">
     <img src="https://www.ecclesiact.com/img/user/assets/valid-xhtml10.png"
     alt="This page IS good HTML according to W3C -
Why not check and see for yourself!"
     title="This page IS good HTML according to W3C -
Why not check and see for yourself!"/>
    </a>
  </p>

</div>

<article>
<h1 id='intro1'>Needls.com Coding Challenge</h1>
<section class='first'>
<h2>Welcome!</h2>
<p>Welcome to our coding challenge, and thank you for your interest in our company.</p>
<p>Please write some code that will solve the following:</p>
<p>We want to add up the total time a given object was in a RUNNING state;<br />
let's ignore what the object is, or what the states mean, irrelevant to this challenge.</p>
<p>The changes to the objects state are recorded in an array, that would be structured as follows</p>

<textarea class='code-php'>
<?php echo "<"."?php\n"; ?>
$statusLog = array(
   array(
      'oldState' => STATE,
      'newState' => STATE,
      'date' => UNIX_TIMESTAMP
   ),
   array(
      'oldState' => STATE,
      'newState' => STATE,
      'date' => UNIX_TIMESTAMP
   )
);
<?php echo "?".">"; ?>
</textarea>

<h2 style='margin:1em 0 0 0' id='intro2'>Status Log Data:</h2>
<ul>
<li>There are three possible states: PAUSED, RUNNING, and COMPLETE.</li>
<li>The array records the unix timestamp (seconds since epoch) of when the object went
into the 'newState', leaving 'oldState'.</li>
<li><b>We want a total sum of the time it spent in a RUNNING state.</b></li>
<li>There may be several state changes, or thousands.</li>
<li>They are not necessarily in order.</li>
<li>An object may have never entered into a RUNNING state.</li>
<li>A COMPLETE state can be treated just as PAUSED, i.e. the object is NOT running.</li>
<li>No guarantees that once an object enters into a COMPLETE state, that it won't then go back into RUNNING!</li>
<li>It's the wild west!</li></ul>

<h2 style='margin:1em 0 0 0' id='intro3'>Start and Stop dates</h2>
<p>To further complicate things, the object may have hard defined 'start' and 'stop' dates
(also in seconds since epoch).</p>
<p>We may have state changes outside of these dates, but want to make sure we only include run times
within these ranges (if given).</p>
<p>The start/stop dates are optional.</p>
<ul>
  <li>An object might have a hard start, but no stop, or a hard stop, but no start.</li>
  <li>Or none.</li>
  <li>Or both.</li>
  <li>A null will be passed in to indicate the absence of a date.</li>
</ul>
</section>

<section id='coding1'>
<h2>Coding Instructions</h2>

<p>The method should take the array of states, an optional start, and an optional stop as parameters,
returning the number of seconds within RUNNING state.</p>
<p>Use constants for the three states so they can easily be changed to match our tests.</p>
<p>Please solve using a function, (or collection) of functions, written in PHP.</p>

<h2 style='margin:2em 0 0 0' id='coding2'>Bonus challenge:</h2>
<p>Make your function easily adapt to running the same calculation for the PAUSED or COMPLETE states.</p>

<h2 style='margin:2em 0 0 0' id='coding3'>Reviewer Notes</h2>
<p style='padding:1em; border: 1px dashed #f00; background: #ff8; text-align: justify'>
All the tests which appear below are now generated using php code and are derived from a mysql database.
All results shown here were generated using the solver code I developed, then automatically checked against the pre-defined
expected results stored in the database to ensure that every case given was correctly solved.<br /><br />
To obtain all source code produced in support of my submission, and to evaluate its quality for yourself, please
<a href="https://bitbucket.org/classaxe/needls/overview" style="color:#00f; text-decoration:underline; font-weight: bold">click here</a>. 
The BitBucket repo includes my sql commands you'll need to create and populate the mysql database you will need to run
this demo yourself.</p>

</section>

<section id='data'>
<h2>Sample Data:</h2>
<p>The following is a collection of data, and expected responses from the function, that I will be testing against.</p> 
<?php echo $Demo->drawTests(); ?>
</section>

<script>
$('.code-php').each(function(index, elem) {
    CodeMirror.fromTextArea(elem, php_spec);
});
</script>

<footer style='text-align:center'>&copy; 2017 Needls.com<br />
<small>(with additional documentation enhancements by Martin Francis)</small>
</footer>
</article>

</body>
</html>