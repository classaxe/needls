/*
  SQL Build Script for Needls Demo
  Author: Martin Francis
  Date: 2017-04-12
*/

/* Create User */
USE `mysql`;
DROP USER IF EXISTS 'needls_demo'@'localhost';
CREATE USER 'needls_demo'@'localhost' IDENTIFIED BY 'MnD56W$90H'; 
FLUSH PRIVILEGES; 
GRANT DELETE, INSERT, SELECT, UPDATE ON `needls_demo`.* TO 'needls_demo'@'localhost'; 
FLUSH PRIVILEGES; 


/* Create Database */
DROP DATABASE IF EXISTS `needls_demo`;
CREATE DATABASE `needls_demo` DEFAULT CHARACTER SET utf8;
USE `needls_demo`;


/* Create Tables with indexes and foreign key constraints */
DROP TABLE IF EXISTS `tests`;
CREATE TABLE `tests` (
  `id` BIGINT(10) UNSIGNED NOT NULL DEFAULT '0',
  `statusLog` TEXT,
  `startDate` VARCHAR(50) NOT NULL DEFAULT '',
  `stopDate` VARCHAR(50) NOT NULL DEFAULT '',
  `expectedResult` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


/* Populate test table with test cases given in document */
INSERT INTO
  `tests`(`id`,`statusLog`,`startDate`,`stopDate`,`expectedResult`)
VALUES
  (1, "array(
    array(
        'date' => date('U', strtotime('2015-10-15')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    )
)", "null", "null", 0
  ),
  (2, "array(
    array(
        'date' => date('U', strtotime('2015-10-15')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    )
)", "date('U', strtotime('next week'))", "null", 0
  ),
  (3, "array(
    array(
        'date' => date('U', strtotime('2015-10-15')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-16')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    )
)", "null", "null", "time() - date('U', strtotime('2015-10-16'))"
  ),
  (4, "array(
    array(
        'date' => date('U', strtotime('2015-10-15')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-16')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
    array(
        'date' => date('U', strtotime('2015-10-17')),
        'oldState' => Settings::CAMPAIGN_STATUS_RUNNING,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-18')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
)", "null", "null", "time() - date('U', strtotime('2015-10-18')) + (24 * 60 * 60)"
  ),
  (5, "array(
    array(
        'date' => date('U', strtotime('2015-10-15')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-16')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
    array(
        'date' => date('U', strtotime('2015-10-17')),
        'oldState' => Settings::CAMPAIGN_STATUS_RUNNING,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-18')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
    array(
        'date' => date('U', strtotime('2015-10-18 12:00:00')),
        'oldState' => Settings::CAMPAIGN_STATUS_RUNNING,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-19')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
)", "null", "null", "time() - date('U', strtotime('2015-10-19')) + (24 * 60 * 60 * 1.5)"
  ),
  (6, "array(
    array(
        'date' => date('U', strtotime('2015-10-15')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-16')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
    array(
        'date' => date('U', strtotime('2015-10-17')),
        'oldState' => Settings::CAMPAIGN_STATUS_RUNNING,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-18')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
    array(
        'date' => date('U', strtotime('2015-10-18 12:00:00')),
        'oldState' => Settings::CAMPAIGN_STATUS_RUNNING,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-19')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
    array(
        'date' => date('U', strtotime('2015-10-20')),
        'oldState' => Settings::CAMPAIGN_STATUS_RUNNING,
        'newState' => Settings::CAMPAIGN_STATUS_COMPLETE
    )
)", "null", "null", "(24 * 60 * 60 * 2.5)"
  ),
  (7, "array(
    array(
        'date' => date('U', strtotime('2015-10-13')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-14')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    )
)", "date('U', strtotime('2015-10-15'))", "null", "time() - date('U', strtotime('2015-10-15'))"
  ),
    (8, "array(
    array(
        'date' => date('U', strtotime('2015-10-13')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-16')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    )
)", "date('U', strtotime('2015-10-15'))", "null", "time() - date('U', strtotime('2015-10-16'))"
  ),
  (9, "array(
    array(
        'date' => date('U', strtotime('2015-10-15')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-16')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
    array(
        'date' => date('U', strtotime('2015-10-17')),
        'oldState' => Settings::CAMPAIGN_STATUS_RUNNING,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-18')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
    array(
        'date' => date('U', strtotime('2015-10-18 12:00:00')),
        'oldState' => Settings::CAMPAIGN_STATUS_RUNNING,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-19')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
)", "date('U', strtotime('2015-10-17'))", "null", "(time() - date('U', strtotime('2015-10-19'))) + (12 * 60 * 60)"
  ),
  (10, "array(
    array(
        'date' => date('U', strtotime('2015-10-13')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-14')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    )
)", "null", "date('U', strtotime('2015-10-15'))", "24 * 60 * 60"
  ),
  (11, "array(
    array(
        'date' => date('U', strtotime('2015-10-13')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-14')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
    array(
        'date' => date('U', strtotime('2015-10-15')),
        'oldState' => Settings::CAMPAIGN_STATUS_RUNNING,
        'newState' => Settings::CAMPAIGN_STATUS_COMPLETE
    )
)", "null", "date('U', strtotime('2015-10-18'))", "24 * 60 * 60"
  ),
  (12, "array(
    array(
        'date' => date('U', strtotime('2015-10-15')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => date('U', strtotime('2015-10-16')),
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    ),
    array(
        'date' => date('U', strtotime('2015-10-17')),
        'oldState' => Settings::CAMPAIGN_STATUS_RUNNING,
        'newState' => Settings::CAMPAIGN_STATUS_RUNNING
    )
)", "null", "null", "time() - date('U', strtotime('2015-10-16'))"
  ),
  (13, "array(
    array(
        'date' => date('U', strtotime('2015-10-15')),
        'oldState' => null,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    ),
    array(
        'date' => $startDate + 1800,
        'oldState' => Settings::CAMPAIGN_STATUS_PAUSED,
        'newState' => Settings::CAMPAIGN_STATUS_PAUSED
    )
)", "null", "null", 0
  );
