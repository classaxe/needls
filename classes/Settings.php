<?php
class Settings
{
    const CAMPAIGN_STATUS_PAUSED =      'PAUSED';
    const CAMPAIGN_STATUS_RUNNING =     'RUNNING';
    const CAMPAIGN_STATUS_COMPLETE =    'COMPLETE';
}
