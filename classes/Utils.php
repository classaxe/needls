<?php
class Utils
{
    public static function formatSeconds($seconds)
    {
        $days = 0;
        $hrs = 0;
        $mins = 0;
        $formatted = "";
        if ($seconds > (60*60*24)) {
            $days = intval($seconds / (60*60*24));
            $seconds = $seconds % (60*60*24);
        }
        if ($seconds > (60*60)) {
            $hrs = intval($seconds / (60*60));
            $seconds = $seconds % (60*60);
        }
        if ($seconds >= 60) {
            $mins = intval($seconds / 60);
            $seconds = $seconds % 60;
        }
        if ($days > 1) {
            return sprintf("%d days, %02d:%02d:%02d (hh:mm:ss)", $days, $hrs, $mins, $seconds);
        }
        if ($days > 0) {
            return sprintf("%d day, %02d:%02d:%02d (hh:mm:ss)", $days, $hrs, $mins, $seconds);
        }
        if ($hrs > 0) {
            return sprintf("%d:%02d:%02d (hh:mm:ss)", $hrs, $mins, $seconds);
        }
        return sprintf("%d:%02d (mm:ss)", $mins, $seconds);
    }
}
