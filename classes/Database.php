<?php
class Database
{
    private $MySQLi;
    private $dsn;

    public function __construct($dsn)
    {
        $this->dsn = $dsn;
        $this->connect();
    }

    private function connect()
    {
        $d = parse_url($this->dsn);
        $d['db'] = trim($d['path'], '/');
        $this->MySQLi = @new \MySQLi($d['host'], $d['user'], $d['pass'], $d['db']);
        if ($this->MySQLi->connect_errno > 0) {
            throw new Exception("<b>Fatal error:</b><br />\n" . $this->MySQLi->connect_error);
        }
    }

    public function getRecordsForSQL($sql)
    {
        if (! $result = $this->MySQLi->query($sql)) {
            return false;
        }
        $out = array();
        while ($row = $result->fetch_assoc()) {
            $out[] = $row;
        }
        return $out;
    }
}
