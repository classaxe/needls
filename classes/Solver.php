<?php
class Solver
{
    private $field_arr;
    private $startDate;
    private $stopDate;
    private $statusLog;
    private $totalTime;

    private $clauseHadTimeBefore =  false;
    private $clauseHadTimeAfter =   false;
    private $clauseHadTimeDuring =  false;

    public function solve($test)
    {
        eval("\$startDate = ".$test['startDate'].";");
        eval("\$stopDate = ". $test['stopDate'].";");
        eval("\$statusLog = ".$test['statusLog'].";");

        $statusLog =    $this->sortRecords($statusLog, array(array('date', 'a')));

        $this->startDate =  $startDate;
        $this->stopDate =   $stopDate;
        $this->statusLog =  $statusLog;


        $this->totalTime =  0;

        if ($this->testNeverWentActive()) {
            return static::showDebug(0, "<li>Campaign never went active.</li>");
        }

        $this->addSecondsBeforeFirstEntry();
        $this->addSecondsAfterLastEntry();
        $this->addSecondsForEachEntry();

        $note = array();
        if ($this->clauseHadTimeBefore) {
            $note[] = "Time was added before log entries";
        }
        if ($this->clauseHadTimeDuring) {
            $note[] = "Time was added during log entries";
        }
        if ($this->clauseHadTimeAfter) {
            $note[] = "Time was added after log entries";
        }
        $notes = "<li>".implode("</li>\n<li>", $note)."</li>\n";
        return static::showDebug((int)$this->totalTime, $notes);
    }

    private function addSecondsAfterLastEntry()
    {
        $entry = $this->statusLog[count($this->statusLog)-1];
        if ($entry['newState'] !== Settings::CAMPAIGN_STATUS_RUNNING) {
            $this->clauseHadTimeAfter = false;
            return;
        }
        if (($this->stopDate === null)) {
            $this->totalTime +=
            time() - ($this->startDate > $entry['date'] ? (int)$this->startDate : (int)$entry['date']);
            $this->clauseHadTimeAfter = true;
            return;
        }
        $this->totalTime +=
        ($this->stopDate > $entry['date'] ? $this->stopDate : time()) - $entry['date'];
        //   (time() - ($this->stopDate > $entry['date'] ? (int)$this->stopDate : (int)$entry['date']));
        $this->clauseHadTimeAfter = true;
    }

    private function addSecondsBeforeFirstEntry()
    {
        $entry = $this->statusLog[0];
        if ($this->startDate !== null &&
                $this->startDate < $entry['date'] &&
                ($entry['oldState'] === null || $entry['oldState'] === Settings::CAMPAIGN_STATUS_RUNNING) &&
                ($entry['newState'] === Settings::CAMPAIGN_STATUS_RUNNING)
                ) {
                    $this->totalTime += $entry['date'] - $this->startDate;
                    $this->clauseHadTimeBefore = true;
        }
                $this->clauseHadTimeBefore = false;
    }

    private function addSecondsForEachEntry()
    {
        $this->clauseHadTimeDuring = false;
        $running = false;
        $segmentStartTime = false;
        $entries = count($this->statusLog);
        for ($i=0; $i<$entries; $i++) {
            $entry = $this->statusLog[$i];
            if ($running &&
                    (($entry['newState'] !== Settings::CAMPAIGN_STATUS_RUNNING) || $i===$entries-1)
                    ) {
                        $this->totalTime+= (int) ($entry['date'] - (int) $segmentStartTime);
                        $this->clauseHadTimeDuring = true;
                        $running = false;
            } elseif ($entry['date'] >= $this->startDate && $entry['newState'] === Settings::CAMPAIGN_STATUS_RUNNING) {
                $segmentStartTime = (int) $entry['date'];
                $running = true;
            }
        }
    }

    private function showDebug($result, $note)
    {
        return array(
                'result' => $result,
                'debug' =>
                "\$startDate =  ".var_export($this->startDate, true)."\n"
                ."\$stopDate =   ".var_export($this->stopDate, true)."\n"
                ."\n"
                ."\$status_log = ".var_export($this->statusLog, true)."\n",
                'note' =>   "<ul class='note'>\n".$note."</ul>"
        );
    }

    private function sortRecords(array $array, array $fieldsort_arr)
    {
        $this->field_arr = $fieldsort_arr;
        usort($array, array($this,'sortRecordsFunction'));
        return $array;
    }

    private function sortRecordsFunction($a, $b)
    {
        foreach ($this->field_arr as $field) {
            switch ($field[1]) { // switch on ascending or descending value
                case "d":
                    if (is_numeric($a[$field[0]]) && is_numeric($b[$field[0]])) {
                        return (float)$b[$field[0]] - (float)$a[$field[0]];
                    } else {
                        $strc = strcmp(strtolower($b[$field[0]]), strtolower($a[$field[0]]));
                        if ($strc != 0) {
                            return $strc;
                        }
                    }
                    break;
                default:
                    if (is_numeric($a[$field[0]]) && is_numeric($b[$field[0]])) {
                        return (float)$a[$field[0]] - (float)$b[$field[0]];
                    } else {
                        $strc = strcmp(strtolower($a[$field[0]]), strtolower($b[$field[0]]));
                        if ($strc != 0) {
                            return $strc;
                        }
                    }
                    break;
            }
        }
        return 0;
    }

    private function testNeverWentActive()
    {
        foreach ($this->statusLog as $entry) {
            if ($entry['newState'] === Settings::CAMPAIGN_STATUS_RUNNING) {
                return false;
            }
        }
        return true;
    }
}
