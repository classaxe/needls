<?php
class Demo
{
    private $database;
    private $solver;
    private $tests;

    public function __construct()
    {
        require_once('config.php');
        $this->database = new Database($dsn);
        $this->solver = new Solver;
        $this->tests = $this->dbGetTests();
    }

    public function drawTestLinks()
    {
        $html = "";
        foreach ($this->tests as $record => $test) {
            $html.= "    <li><a href=\"#data".$test['id']."\">Data ".$test['id']."</a>";
        }
        return $html;
    }

    private function drawTest($test)
    {
        $solution = $this->solver->solve($test);

        $a = $solution['result'];
        eval('$q='.$test['expectedResult'].';');

        return
             "<h3 id='data".$test['id']."'>Data ".$test['id']."</h3>\n"
            ."<textarea class='code-php'>\n"
            ."<"."?php\n"
            ."\$StartDate = ".$test['startDate'].";\n"
            ."\$StopDate = ".$test['stopDate'].";\n"
            ."\n"
            ."\$statusLog = ".$test['statusLog']."\n"
            ."?"."></textarea>\n"
            ."<p><b>Expected Result:</b><br />\n"
            ." &nbsp; &nbsp; ".$test['expectedResult']."<br />\n"
            ." &nbsp; &nbsp; ".number_format($q)." second".($q===1 ? '' : 's')."<br />\n"
            ." &nbsp; &nbsp; ".Utils::formatSeconds($q)
            ."</p>\n"
            ."<p><b>Test Result:</b><br />"
            ." &nbsp; &nbsp; "
            .((int)$a === (int)$q ?
                "<span class='pass'>PASS (".number_format($a)." === ".number_format($q).")</span>"
             :
                "<span class='fail'>FAIL (".number_format($a)." !== ".number_format($q).")</span>"
            )
            ."</p>\n"
            ."<p><b>Computed Result:</b><br />\n"
            ." &nbsp; &nbsp; "
            ."<span class='pass'>".number_format($a). " second".($a===1 ? '' : 's')."</span>"
            ."<br />\n"
            ." &nbsp; &nbsp; "
            ."<span class='pass'>".Utils::formatSeconds($a)."</span>"
            ."</p>\n"
            ."<p><b>About Computed Result:</b></p>\n"
            .$solution['note']
            ."<p><b>Debug:</b><br style='clear:both'>\n"
            ." &nbsp; &nbsp; <a href='#' onclick=\"\$('pre.debug').toggle(); return false;\" class='toggle'>"
            ."Show / Hide</a>"
            ."</p>\n"
            ."<pre class='debug'>"
            .$solution['debug']
            ."</pre>";
    }

    public function drawTests()
    {
        $html = "";
        $i = 1;
        foreach ($this->tests as $record => $test) {
        // Uncomment the next line to trial only specified test cases
        //   if (in_array($i, array(9,10)))
                $html.= $this->drawTest($test);
            $i++;
        }
        return $html;
    }

    private function dbGetTests()
    {
        $sql =
             "SELECT\n"
            ."  *\n"
            ."FROM\n"
            ."  `tests`\n"
            ."ORDER BY\n"
            ."  `id`";
        return $this->database->getRecordsForSQL($sql);
    }
}
